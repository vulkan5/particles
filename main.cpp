/*
Prefixes: 

    num_* : count
    ci_*  : Vulkan CreateInfo structure
    ai_*  : Vulkan AllocateInfo structure
    bi_*  : Vulkan BeginInfo structure
    vk_*  : Vulkan object handle


Possible TODO:

*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <cstring>

#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

extern "C"
{
    // struct Buffer;
    // void create_buffer(const VkDevice vk_device, const VkPhysicalDeviceMemoryProperties* vk_phys_dev_mem_props, const VkBufferUsageFlags usage, const VkMemoryPropertyFlags properties, struct Buffer* buffer);
    #include "buffer/buffer.h"
};



#define VK_CHECK( str, result ) do { \
   if (result != VK_SUCCESS) \
    printf("%s: result != VK_SUCCESS", str); \
} while ( 0 ) \

static constexpr int WIN_WIDTH  = 1500;
static constexpr int WIN_HEIGHT = 1000;
static constexpr int NUM_FRAMES_IN_FLIGHT = 3;
static constexpr int WORKGROUP_SIZE = 16;

struct Particle
{
    alignas(16) float col[4];
    alignas(8)  float pos[2];
    alignas(8)  float vel[2];
};

struct Push_Constant
{
   float cursor_pos[2];
   float time;
   bool cursor_pressed;
} push_constant;


VkInstance create_instance(const uint32_t num_instance_extensions, const char** instance_extensions, const uint32_t num_instance_layers, const char** instance_layers) 
{
    VkApplicationInfo vk_app_info;
    vk_app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    vk_app_info.pNext = nullptr;
    vk_app_info.pApplicationName = "Compute App";
    vk_app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    vk_app_info.pEngineName = "Julia";
    vk_app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    vk_app_info.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo ci_instance;
    ci_instance.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    ci_instance.pNext = nullptr;
    ci_instance.flags = 0x0;
    ci_instance.pApplicationInfo = &vk_app_info;
    ci_instance.enabledExtensionCount = num_instance_extensions;
    ci_instance.ppEnabledExtensionNames = instance_extensions;
    ci_instance.enabledLayerCount = num_instance_layers;
    ci_instance.ppEnabledLayerNames = instance_layers;

    VkInstance vk_instance = VK_NULL_HANDLE;
    VK_CHECK("instance creation", vkCreateInstance(&ci_instance, NULL, &vk_instance));

    return vk_instance;
}

VkShaderModule create_shader_module(VkDevice vk_device, const char* filename)
{
    FILE* f = fopen(filename, "r");
    if (f == NULL)
    {
        printf("Failed to open file %s!\n", filename);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    const size_t nbytes_file_size = (size_t)ftell(f);
    rewind(f);

    uint32_t* buffer = (uint32_t*)malloc(nbytes_file_size);
    fread(buffer, nbytes_file_size, 1, f);
    fclose(f);

    VkShaderModuleCreateInfo ci_shader_module;
    ci_shader_module.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    ci_shader_module.pNext = NULL;
    ci_shader_module.flags = 0x0;
    ci_shader_module.codeSize = nbytes_file_size;
    ci_shader_module.pCode = buffer;

    VkShaderModule vk_shader_module;
    VkResult result = vkCreateShaderModule(vk_device, &ci_shader_module, NULL, &vk_shader_module);
    if (result != VK_SUCCESS)
    {
        printf("Failed to create shader module for %s!\n", filename);
        exit(EXIT_FAILURE);
    }

    free(buffer);

    return vk_shader_module;
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    push_constant.cursor_pos[0] = 2 * xpos / WIN_WIDTH - 1;
    push_constant.cursor_pos[1] = 2 * ypos / WIN_HEIGHT - 1;
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
    {
        push_constant.cursor_pressed = true;
    }
    else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
    {
        push_constant.cursor_pressed = false;
    }
}


int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow* glfw_window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "Compute Test", nullptr, nullptr);
    glfwSetCursorPosCallback(glfw_window, cursor_position_callback);
    glfwSetMouseButtonCallback(glfw_window, mouse_button_callback);


    //*** Instance 
    uint32_t num_instance_extensions = 0;
    const char** instance_extensions = glfwGetRequiredInstanceExtensions(&num_instance_extensions);

    const uint32_t num_instance_layers = 1;
    const char* instance_layers[1] = { "VK_LAYER_KHRONOS_validation" };
    VkInstance vk_instance = create_instance(num_instance_extensions, instance_extensions, num_instance_layers, instance_layers);

    //*** Surface
    VkSurfaceKHR vk_surface = VK_NULL_HANDLE;
    VK_CHECK("surface creation", glfwCreateWindowSurface(vk_instance, glfw_window, nullptr, &vk_surface));

    //*** Physical Device
    VkPhysicalDevice vk_physical_device = VK_NULL_HANDLE;
    VkPhysicalDeviceProperties vk_physical_device_properties = {};
    VkPhysicalDeviceMemoryProperties vk_physical_device_memory_properties = {};

    {
        uint32_t num_physical_devices = 0;
        vkEnumeratePhysicalDevices(vk_instance, &num_physical_devices, nullptr);
        VkPhysicalDevice* vk_physical_device_list = new VkPhysicalDevice[num_physical_devices];
        vkEnumeratePhysicalDevices(vk_instance, &num_physical_devices, vk_physical_device_list);

        vk_physical_device = vk_physical_device_list[1];
        delete [] vk_physical_device_list;
    }

    vkGetPhysicalDeviceProperties(vk_physical_device, &vk_physical_device_properties);
    vkGetPhysicalDeviceMemoryProperties(vk_physical_device, &vk_physical_device_memory_properties);
    printf( "Selected Physical Device: %s\n", vk_physical_device_properties.deviceName);

    //*** Choosing Queue Families
    uint32_t graphics_q_index = UINT32_MAX;
    uint32_t present_q_index  = UINT32_MAX;
    uint32_t compute_q_index  = UINT32_MAX;

    VkQueue vk_compute_q  = VK_NULL_HANDLE;
    VkQueue vk_graphics_q = VK_NULL_HANDLE;

    {
        uint32_t num_q_family_properties = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(vk_physical_device, &num_q_family_properties, nullptr);
        VkQueueFamilyProperties* vk_q_family_properties = new VkQueueFamilyProperties[num_q_family_properties];
        vkGetPhysicalDeviceQueueFamilyProperties(vk_physical_device, &num_q_family_properties, vk_q_family_properties);

        bool graphics_and_present_found = false;
        for (uint32_t i = 0; i < num_q_family_properties; ++i)
        {
            VkBool32 q_fam_supports_present = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(vk_physical_device, i, vk_surface, &q_fam_supports_present);

            if (vk_q_family_properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT && q_fam_supports_present == VK_TRUE)
            {
                graphics_q_index = i;
                present_q_index = i;
                break;
            }
        }

        for (uint32_t i = 0; i < num_q_family_properties; ++i)
        {
            if (vk_q_family_properties[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
            {
                compute_q_index = i;
            }
        }

        delete [] vk_q_family_properties;

        assert(graphics_q_index < UINT32_MAX && "no supported graphics queue family index");
        assert(present_q_index < UINT32_MAX && "no supported present queue family index");
        assert(compute_q_index < UINT32_MAX && "no supported compute queue family index");

        printf("Graphics Queue Family Index: %u\n", graphics_q_index);
        printf("Present Queue Family Index:  %u\n", present_q_index);
        printf("Compute Queue Family Index:  %u\n", compute_q_index);
    }

    const bool compute_q_same_as_graphics = (graphics_q_index | compute_q_index) == graphics_q_index;
    printf("Compute and Graphics in the same Queue Family: %i\n", compute_q_same_as_graphics);
    const uint32_t num_queues = compute_q_same_as_graphics ? 1 : 2;


    //*** Logical Device
    VkDevice vk_device = VK_NULL_HANDLE;

    {
        const uint32_t num_device_extensions = 1;
        const char* device_extensions[1] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
        const float q_priority = 1.0f;

        if (num_queues == 1)
        {
            VkDeviceQueueCreateInfo ci_device_q_list[1];
            ci_device_q_list[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            ci_device_q_list[0].pNext = nullptr;
            ci_device_q_list[0].flags = 0x0;
            ci_device_q_list[0].queueFamilyIndex = graphics_q_index;
            ci_device_q_list[0].queueCount = 1;
            ci_device_q_list[0].pQueuePriorities = &q_priority;

            VkDeviceCreateInfo ci_device;
            ci_device.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
            ci_device.pNext = nullptr;
            ci_device.flags = 0x0;
            ci_device.queueCreateInfoCount = 1;
            ci_device.pQueueCreateInfos = ci_device_q_list;
            ci_device.enabledLayerCount = 0;
            ci_device.ppEnabledLayerNames = nullptr;
            ci_device.enabledExtensionCount = num_device_extensions;
            ci_device.ppEnabledExtensionNames = device_extensions;
            ci_device.pEnabledFeatures = nullptr;

            VK_CHECK("logical device creation", vkCreateDevice(vk_physical_device, &ci_device, nullptr, &vk_device));
        }
        else
        {
            VkDeviceQueueCreateInfo* ci_device_q_list = new VkDeviceQueueCreateInfo[2];
            ci_device_q_list[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            ci_device_q_list[0].pNext = nullptr;
            ci_device_q_list[0].flags = 0x0;
            ci_device_q_list[0].queueFamilyIndex = graphics_q_index;
            ci_device_q_list[0].queueCount = 1;
            ci_device_q_list[0].pQueuePriorities = &q_priority;

            ci_device_q_list[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            ci_device_q_list[1].pNext = nullptr;
            ci_device_q_list[1].flags = 0x0;
            ci_device_q_list[1].queueFamilyIndex = compute_q_index;
            ci_device_q_list[1].queueCount = 1;
            ci_device_q_list[1].pQueuePriorities = &q_priority;

            VkDeviceCreateInfo ci_device;
            ci_device.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
            ci_device.pNext = nullptr;
            ci_device.flags = 0x0;
            ci_device.queueCreateInfoCount = 2;
            ci_device.pQueueCreateInfos = ci_device_q_list;
            ci_device.enabledLayerCount = 0;
            ci_device.ppEnabledLayerNames = nullptr;
            ci_device.enabledExtensionCount = num_device_extensions;
            ci_device.ppEnabledExtensionNames = device_extensions;
            ci_device.pEnabledFeatures = nullptr;
            
            VK_CHECK("logical device creation", vkCreateDevice(vk_physical_device, &ci_device, nullptr, &vk_device));
        }

        vkGetDeviceQueue(vk_device, compute_q_index, 0, &vk_compute_q);
        vkGetDeviceQueue(vk_device, graphics_q_index, 0, &vk_graphics_q);
    }

    //*** Swapchain creation
    VkSwapchainKHR vk_swapchain = VK_NULL_HANDLE;
    VkSwapchainCreateInfoKHR ci_swapchain;
    VkImage* vk_swapchain_image_list = nullptr;
    VkImageView* vk_swapchain_image_view_list = nullptr;
    uint32_t num_swapchain_images = 0;

    {
        const uint32_t requested_image_count = 3;
        const VkFormat requested_surface_format = VK_FORMAT_A1R5G5B5_UNORM_PACK16;
        const VkExtent2D requested_image_extend2D = { WIN_WIDTH, WIN_HEIGHT};

        ci_swapchain.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        ci_swapchain.pNext = nullptr;
        ci_swapchain.flags = 0x0;
        ci_swapchain.surface = vk_surface;

        VkSurfaceCapabilitiesKHR vk_surface_capabilities;
        VK_CHECK("querying phys dev surface caps", vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vk_physical_device, 
            vk_surface, &vk_surface_capabilities));

        //** Image Count
        {
            assert(requested_image_count > 0 && "Invalid requested image count for swapchain!");

            // If the minImageCount is 0, then there is not a limit on the number of images the swapchain
            // can support (ignoring memory constraints). See the Vulkan Spec for more information.
            if (vk_surface_capabilities.maxImageCount == 0)
            {
                if (requested_image_count >= vk_surface_capabilities.minImageCount)
                {
                    ci_swapchain.minImageCount = requested_image_count;
                }
                else
                {
                    printf("Failed to create Swapchain. The requested number of images %u does not meet the minimum requirement of %u.\n", requested_image_count, vk_surface_capabilities.minImageCount);
                    exit(EXIT_FAILURE);
                }
            }
            else if (requested_image_count >= vk_surface_capabilities.minImageCount &&
                     requested_image_count <= vk_surface_capabilities.maxImageCount)
            {
                ci_swapchain.minImageCount = requested_image_count;
            }
            else
            {
                printf("The number of requested Swapchain images %u is not supported. Min: %u\t Max: %u\n", requested_image_count, vk_surface_capabilities.minImageCount, vk_surface_capabilities.maxImageCount);
                exit(EXIT_FAILURE);
            }
        }

        //*** Image Format
        {
            uint32_t num_supported_surface_formats = 0;
            VK_CHECK("querying num supported surface formats", vkGetPhysicalDeviceSurfaceFormatsKHR(vk_physical_device, vk_surface, &num_supported_surface_formats, nullptr));
            VkSurfaceFormatKHR* vk_supported_surface_format_list = new VkSurfaceFormatKHR[num_supported_surface_formats];
            VK_CHECK("querying supported surface formats", vkGetPhysicalDeviceSurfaceFormatsKHR(vk_physical_device, vk_surface, &num_supported_surface_formats, vk_supported_surface_format_list));

            bool requestedFormatFound = false;
            for (uint32_t i = 0; i < num_supported_surface_formats; ++i)
            {
                if (vk_supported_surface_format_list[i].format == requested_surface_format)
                {
                    ci_swapchain.imageFormat = vk_supported_surface_format_list[i].format;
                    ci_swapchain.imageColorSpace = vk_supported_surface_format_list[i].colorSpace;
                    requestedFormatFound = true;
                    break;
                }
            }

            if (!requestedFormatFound)
            {
                ci_swapchain.imageFormat = vk_supported_surface_format_list[0].format;
                ci_swapchain.imageColorSpace = vk_supported_surface_format_list[0].colorSpace;
                printf("WARNING - Requested format %u is not avaliable! Defaulting to first avaliable.\n", (unsigned int)requested_surface_format);
            }

            delete [] vk_supported_surface_format_list;
        }

        //*** Extent (size)
        {
            // The Vulkan Spec states that if the current width/height is 0xFFFFFFFF, then the surface size
            // will be deteremined by the extent specified in the VkSwapchainCreateInfoKHR.
            if (vk_surface_capabilities.currentExtent.width != (uint32_t)-1)
            {
                ci_swapchain.imageExtent = requested_image_extend2D;
            }
            else
            {
                ci_swapchain.imageExtent = vk_surface_capabilities.currentExtent;
            }
        }

        ci_swapchain.imageArrayLayers = 1;
        ci_swapchain.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        ci_swapchain.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        ci_swapchain.queueFamilyIndexCount = 0;
        ci_swapchain.pQueueFamilyIndices = nullptr;

        //** Pre Transform
        {
            if (vk_surface_capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
            {
                ci_swapchain.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
            }
            else
            {
                ci_swapchain.preTransform = vk_surface_capabilities.currentTransform;
                printf("WARNING - Swapchain pretransform is not IDENTITIY_BIT_KHR!\n");
            }
        }

        //** Composite Alpha
        {
            // Determine the composite alpha format the application needs.
        	// Find a supported composite alpha format (not all devices support alpha opaque),
            // but we prefer it.
        	// Simply select the first composite alpha format available
            // Used for blending with other windows in the system
        	VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = {
        		VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        		VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
        		VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
        		VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
        	};
            for (size_t i = 0; i < 4; ++i)
            {
        		if (vk_surface_capabilities.supportedCompositeAlpha & compositeAlphaFlags[i]) 
                {
                    ci_swapchain.compositeAlpha = compositeAlphaFlags[i];
                    break;
        		};
            }
        }

        //** Present Mode
        {
            uint32_t num_supported_present_modes = 0;
            VK_CHECK("querying num present modes", vkGetPhysicalDeviceSurfacePresentModesKHR(vk_physical_device, vk_surface, &num_supported_present_modes, nullptr));
            VkPresentModeKHR* vk_supported_present_modes = new VkPresentModeKHR[num_supported_present_modes];
            VK_CHECK("querying prenset modes", vkGetPhysicalDeviceSurfacePresentModesKHR(vk_physical_device, vk_surface, &num_supported_present_modes, vk_supported_present_modes));

            // Determine the present mode the application needs.
            // Try to use mailbox, it is the lowest latency non-tearing present mode
            // All devices support FIFO (this mode waits for the vertical blank or v-sync)
            ci_swapchain.presentMode = VK_PRESENT_MODE_FIFO_KHR;
            for (uint32_t i = 0; i < num_supported_present_modes; ++i)
            {
                if (vk_supported_present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
                {
                    ci_swapchain.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
                    break;
                }
            }

            delete [] vk_supported_present_modes;
            printf("Swapchain present mode: %d\n", static_cast<uint32_t>(ci_swapchain.presentMode));
        }

        //*** Creating Swapchain
        ci_swapchain.clipped = VK_TRUE;
        ci_swapchain.oldSwapchain = VK_NULL_HANDLE;
        VK_CHECK("creating swapchain", vkCreateSwapchainKHR(vk_device, &ci_swapchain, nullptr, &vk_swapchain));

        //*** Get Images/ImageViews
        {
            VK_CHECK("getting swapchain image count", vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &num_swapchain_images, nullptr));
            vk_swapchain_image_list = new VkImage[num_swapchain_images];
            vk_swapchain_image_view_list = new VkImageView[num_swapchain_images];
            VK_CHECK("getting swapchain images", vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &num_swapchain_images, vk_swapchain_image_list));

            for (size_t i = 0; i < num_swapchain_images; ++i)
            {
                VkImageViewCreateInfo vk_colorAttachmentView;
                vk_colorAttachmentView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                vk_colorAttachmentView.pNext = nullptr;
                vk_colorAttachmentView.flags = 0;
                vk_colorAttachmentView.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                vk_colorAttachmentView.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                vk_colorAttachmentView.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                vk_colorAttachmentView.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                vk_colorAttachmentView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                vk_colorAttachmentView.subresourceRange.baseMipLevel = 0;
                vk_colorAttachmentView.subresourceRange.levelCount = 1;
                vk_colorAttachmentView.subresourceRange.baseArrayLayer = 0;
                vk_colorAttachmentView.subresourceRange.layerCount = 1;
                vk_colorAttachmentView.viewType = VK_IMAGE_VIEW_TYPE_2D;
                vk_colorAttachmentView.image = vk_swapchain_image_list[i];
                vk_colorAttachmentView.format = ci_swapchain.imageFormat;

                VK_CHECK("creating image view for swapchain image", vkCreateImageView(vk_device, &vk_colorAttachmentView, nullptr, &vk_swapchain_image_view_list[i]));
            }
        }
    }

    //*** RenderPass creation
    VkRenderPass vk_render_pass = VK_NULL_HANDLE;

    {
        VkAttachmentDescription attachments[1] = {};

        // Color
        attachments[0].flags = 0x0;
        attachments[0].format = ci_swapchain.imageFormat;
        attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        // Depth
        // attachments[1].flags = 0x0;
        // attachments[1].format = VK_FORMAT_D16_UNORM;
        // attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
        // attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        // attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        // attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        // attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        // attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        // attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        /*
         * VkAttachmentReference {
         *      .attachment - integer value corresponding to index in VkRenderPassCreateInfo.pAttachments
         *      .layout - the layout the attachment uses during the subpass
         * }
        */

        VkAttachmentReference color_reference;
        color_reference.attachment = 0;
        color_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        // VkAttachmentReference depth_reference;
        // depth_reference.attachment = 1;
        // depth_reference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        /*
         * VkSubpassDescription {
         *      .pipelineBindPoint - Graphics/Compute
         *      .inputAttachments - attachments the subpass reads from
         *      .colorAttachments - attachments the subpass writes to
         *      .depthStencilAttachments - depth/stencil attachment this subpass writes to
         * } 
        */

        VkSubpassDescription subpass;
        subpass.flags = 0x0;
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.inputAttachmentCount = 0;
        subpass.pInputAttachments = nullptr;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &color_reference;
        subpass.pResolveAttachments = nullptr;
        subpass.pDepthStencilAttachment = nullptr; // &depth_reference;
        subpass.preserveAttachmentCount = 0;
        subpass.pPreserveAttachments = nullptr;

       /*
        * VkSubpassDependency {
        *       .srcSubpass - subpass index of the first subpass in the dependency (or EXTERNAL)
        *       .dstSubpass - subpass index of the second subpass in the dependency (or EXTERNAL)
        *       .srcStageMask - the first sync scope only includes execution of these stages
        *       .dstStageMask - the second sync scope only includes execution of these stages
        *       .srcAccessMask - memory access available to the srcStageMask
        *       .dstAccessMask - memory access available to the dstStageMask
        *       .dependencyFlags - by Region means Frambuffer local
        * } 
       */

    	VkSubpassDependency dependencies[2];

    	// First dependency at the start of the renderpass
    	// Does the transition from final to initial layout
    	dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;                             // Producer of the dependency
    	dependencies[0].dstSubpass = 0;                                               // Consumer is our single subpass that will wait for the execution dependency
    	dependencies[0].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; // Match our pWaitDstStageMask when we vkQueueSubmit
    	dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; // is a loadOp stage for color attachments
    	dependencies[0].srcAccessMask = 0;                                            // semaphore wait already does memory dependency for us
    	dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;         // is a loadOp CLEAR access mask for color attachments
    	dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

    	// Second dependency at the end the renderpass
    	// Does the transition from the initial to the final layout
    	// Technically this is the same as the implicit subpass dependency, but we are gonna state it explicitly here
    	dependencies[1].srcSubpass = 0;                                               // Producer of the dependency is our single subpass
    	dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;                             // Consumer are all commands outside of the renderpass
    	dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT; // is a storeOp stage for color attachments
    	dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;          // Do not block any subsequent work
    	dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;         // is a storeOp `STORE` access mask for color attachments
    	dependencies[1].dstAccessMask = 0;
    	dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        /*
         * VkRenderPassCreateInfo {
         *      .attachments - the attachments used in this render pass
         *      .subpasses - the subpasses used in this render pass
         *      .dependencies - the subpass dependencies for this render pass
         * }
        */

        VkRenderPassCreateInfo ci_renderPass;
        ci_renderPass.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        ci_renderPass.pNext = nullptr;
        ci_renderPass.flags = 0x0;
        ci_renderPass.attachmentCount = 1; // 2;
        ci_renderPass.pAttachments = attachments;
        ci_renderPass.subpassCount = 1;
        ci_renderPass.pSubpasses = &subpass;
        ci_renderPass.dependencyCount = 2;
        ci_renderPass.pDependencies = dependencies;

        VK_CHECK("creating renderpass", vkCreateRenderPass(vk_device, &ci_renderPass, nullptr, &vk_render_pass));
    }

    //*** Framebuffer creation
    VkFramebuffer vk_framebuffer_list[NUM_FRAMES_IN_FLIGHT];

    {
        for (size_t i = 0; i < NUM_FRAMES_IN_FLIGHT; ++i)
        {
            VkImageView attachments[1];
            attachments[0] = vk_swapchain_image_view_list[i];
            // attachments[1] = vkm_depthStencilAttachment.vk_depthImageView;

            VkFramebufferCreateInfo ci_frameBuffer = {};
            ci_frameBuffer.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            ci_frameBuffer.renderPass = vk_render_pass;
            ci_frameBuffer.attachmentCount = 1;
            ci_frameBuffer.pAttachments = attachments;
            ci_frameBuffer.width = ci_swapchain.imageExtent.width;
            ci_frameBuffer.height = ci_swapchain.imageExtent.height;
            ci_frameBuffer.layers = 1;
            
            VK_CHECK("creating framebuffer", vkCreateFramebuffer(vk_device, &ci_frameBuffer, nullptr, 
                &vk_framebuffer_list[i]));
        }
    }


    //*** ---------- Pre-Init Done ---------- ***// 

    //*** Command Buffer Creation
    VkCommandPool vk_compute_cmd_pool = VK_NULL_HANDLE;
    VkCommandBuffer vk_compute_cmd_buffer = VK_NULL_HANDLE;
    VkCommandBufferBeginInfo bi_compute_cmd_buffer;

    VkCommandPool vk_graphics_cmd_pool = VK_NULL_HANDLE;
    VkCommandBuffer vk_graphics_cmd_buffer = VK_NULL_HANDLE;
    VkCommandBufferBeginInfo bi_graphics_cmd_buffer;

    {
        //*** Compute
        {
            //*** Allocating compute command pool
            {
                VkCommandPoolCreateInfo ci_command_pool;
                ci_command_pool.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                ci_command_pool.pNext = nullptr;
                ci_command_pool.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
                ci_command_pool.queueFamilyIndex = compute_q_index;

                VK_CHECK("compute command pool creation", vkCreateCommandPool(vk_device, &ci_command_pool,
                    nullptr, &vk_compute_cmd_pool));
            }

            //*** Allocating and recording compute command buffer(s)
            {
                VkCommandBufferAllocateInfo ai_cmd_buffer;
                ai_cmd_buffer.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                ai_cmd_buffer.pNext = nullptr;
                ai_cmd_buffer.commandPool = vk_compute_cmd_pool;
                ai_cmd_buffer.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
                ai_cmd_buffer.commandBufferCount = 1;

                VK_CHECK("allocating compute command buffers", vkAllocateCommandBuffers(vk_device,
                    &ai_cmd_buffer, &vk_compute_cmd_buffer));
            }

            bi_compute_cmd_buffer.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            bi_compute_cmd_buffer.pNext = nullptr;
            bi_compute_cmd_buffer.flags = 0x0; // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            bi_compute_cmd_buffer.pInheritanceInfo = nullptr;
        }

        //*** Graphics
        {
            //** Allocating graphics command pool
            {
                VkCommandPoolCreateInfo ci_command_pool;
                ci_command_pool.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
                ci_command_pool.pNext = nullptr;
                ci_command_pool.flags = 0x0;
                ci_command_pool.queueFamilyIndex = graphics_q_index;

                VK_CHECK("compute command pool creation", vkCreateCommandPool(vk_device, 
                    &ci_command_pool, nullptr, &vk_graphics_cmd_pool));
            }

            //*** Allocating graphics command buffer(s)
            {
                VkCommandBufferAllocateInfo ai_cmd_buffer;
                ai_cmd_buffer.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
                ai_cmd_buffer.pNext = nullptr;
                ai_cmd_buffer.commandPool = vk_graphics_cmd_pool;
                ai_cmd_buffer.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
                ai_cmd_buffer.commandBufferCount = 1;

                VK_CHECK("allocating compute command buffers", vkAllocateCommandBuffers(vk_device,
                    &ai_cmd_buffer, &vk_graphics_cmd_buffer));
            }

            bi_graphics_cmd_buffer.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            bi_graphics_cmd_buffer.pNext = nullptr;
            bi_graphics_cmd_buffer.flags = 0x0; // VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
            bi_graphics_cmd_buffer.pInheritanceInfo = nullptr;
        }
    }


    //*** Sync Primitives Creation
    VkSemaphore vk_compute_sem_work_done = VK_NULL_HANDLE;

    VkSemaphore vk_graphics_sem_render_done = VK_NULL_HANDLE;
    VkSemaphore vk_graphics_sem_image_acquired = VK_NULL_HANDLE;
    VkFence vk_graphics_fence = VK_NULL_HANDLE;

    {
        //*** Compute
        {
            VkSemaphoreCreateInfo ci_sem;
            ci_sem.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            ci_sem.pNext = nullptr;
            ci_sem.flags = 0x0;

            VK_CHECK("creating compute work done sem", vkCreateSemaphore(vk_device, &ci_sem, nullptr, 
                &vk_compute_sem_work_done));
        }

        //*** Graphics
        {
            VkFenceCreateInfo ci_fence;
            ci_fence.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            ci_fence.pNext = nullptr;
            ci_fence.flags = VK_FENCE_CREATE_SIGNALED_BIT;

            VK_CHECK("creating graphics fence", vkCreateFence(vk_device, &ci_fence, nullptr, &vk_graphics_fence));

            VkSemaphoreCreateInfo ci_sem;
            ci_sem.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
            ci_sem.pNext = nullptr;
            ci_sem.flags = 0x0;

            VK_CHECK("creating graphics img acuired sem", vkCreateSemaphore(vk_device, &ci_sem, nullptr, 
                &vk_graphics_sem_image_acquired));

            VK_CHECK("creating graphics rendering complete sem", vkCreateSemaphore(vk_device, &ci_sem, nullptr, 
                &vk_graphics_sem_render_done));
        }
    }

    //*** Scene Data Init
    struct Buffer storage_buffer;
    constexpr size_t num_rings = 2000;
    constexpr size_t num_particles_in_ring = 2500;
    constexpr size_t num_particles = num_rings * num_particles_in_ring;
    constexpr uint32_t num_workgroups = num_particles / 32 + 1;

    {
        Particle* particle_array = static_cast<Particle*>(malloc(num_particles * sizeof(Particle)));
        constexpr float theta_step = 2 * 3.14 / num_particles_in_ring;
        constexpr float radius_step = 1.f / num_rings;

        for (size_t i = 0; i < num_rings; i++)
        {
            const float radius = (i + 1) * radius_step;
            for (size_t j = 0; j < num_particles_in_ring; j++)
            {
                const size_t index = i * num_particles_in_ring + j;
                //* constants
                {
                    particle_array[index].pos[0] = i * radius_step * cosf(j * theta_step);
                    particle_array[index].pos[1] = i * radius_step * sinf(j * theta_step);
                    // particle_array[j].col[0] = 0.0f;
                    // particle_array[j].col[1] = 1.0f;
                    // particle_array[j].col[2] = 0.0f;
                    // particle_array[j].alpha  = 1.0f;
                }

                // particle_array[index].theta = j * theta_step;
                // particle_array[index].radius = radius;
            }
        }

        struct Buffer staging_buffer;
        staging_buffer.offset = 0;
        staging_buffer.size = sizeof(struct Particle) * num_particles;

        storage_buffer.offset = 0;
        storage_buffer.size = sizeof(struct Particle) * num_particles;

        create_buffer(vk_device, &vk_physical_device_memory_properties,
                      VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                      VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
                      &staging_buffer);

        void* pData = nullptr;
        VK_CHECK("staging buffer vkMapMemory()", vkMapMemory(vk_device, staging_buffer.vk_device_memory, 
                 staging_buffer.offset, staging_buffer.size, 0x0, &pData));
            memcpy(pData, particle_array, staging_buffer.size);
        vkUnmapMemory(vk_device, staging_buffer.vk_device_memory);

        create_buffer(vk_device, &vk_physical_device_memory_properties,
                      VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                      &storage_buffer);

         VkBufferCopy vk_buffer_copy;
         vk_buffer_copy.size = staging_buffer.size;
         vk_buffer_copy.dstOffset = 0;
         vk_buffer_copy.srcOffset = 0;

         VK_CHECK("begin staging buffer cmd buff record", vkBeginCommandBuffer(vk_graphics_cmd_buffer, &bi_graphics_cmd_buffer));

            vkCmdCopyBuffer(vk_graphics_cmd_buffer, staging_buffer.vk_buffer, storage_buffer.vk_buffer, 1, &vk_buffer_copy);

         VK_CHECK("end staging buffer cmd buff record", vkEndCommandBuffer(vk_graphics_cmd_buffer));

         VkSubmitInfo vk_submit_info;
         vk_submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
         vk_submit_info.pNext = nullptr;
         vk_submit_info.waitSemaphoreCount = 0;
         vk_submit_info.pWaitSemaphores = nullptr;
         vk_submit_info.pWaitDstStageMask = 0x0;
         vk_submit_info.commandBufferCount = 1;
         vk_submit_info.pCommandBuffers = &vk_graphics_cmd_buffer;
         vk_submit_info.signalSemaphoreCount = 0;
         vk_submit_info.pSignalSemaphores = nullptr;

         VK_CHECK("resetting staging buffer fence", vkResetFences(vk_device, 1, &vk_graphics_fence));
         VK_CHECK("staging buffer transfer vkQueueSubmit()", vkQueueSubmit(vk_graphics_q, 1, &vk_submit_info, vk_graphics_fence));
         VK_CHECK("waiting on staging buffer fence", vkWaitForFences(vk_device, 1, &vk_graphics_fence, VK_TRUE, UINT64_MAX));
         VK_CHECK("restting graphics cmd pool after staging buffer transfer", vkResetCommandPool(vk_device, vk_graphics_cmd_pool, 0x0));

         vkDestroyBuffer(vk_device, staging_buffer.vk_buffer, nullptr);
         vkFreeMemory(vk_device, staging_buffer.vk_device_memory, nullptr);
         free(particle_array);
    }

    //*** Descriptor Creation (Graphics + Compute)
    VkDescriptorSetLayout vk_desc_set_layout = VK_NULL_HANDLE;
    VkDescriptorPool vk_desc_pool = VK_NULL_HANDLE;
    VkDescriptorSet vk_desc_set = VK_NULL_HANDLE;

    {
        //*** Layout
        {
            VkDescriptorSetLayoutBinding vk_desc_set_layout_binding_list[1];
            vk_desc_set_layout_binding_list[0].binding = 0;
            vk_desc_set_layout_binding_list[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
            vk_desc_set_layout_binding_list[0].descriptorCount = 1;
            vk_desc_set_layout_binding_list[0].stageFlags = VK_SHADER_STAGE_COMPUTE_BIT | VK_SHADER_STAGE_VERTEX_BIT;
            vk_desc_set_layout_binding_list[0].pImmutableSamplers = nullptr;

            VkDescriptorSetLayoutCreateInfo ci_desc_set_layout;
            ci_desc_set_layout.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
            ci_desc_set_layout.pNext = nullptr;
            ci_desc_set_layout.flags = 0x0;
            ci_desc_set_layout.bindingCount = 1;
            ci_desc_set_layout.pBindings = vk_desc_set_layout_binding_list;

            VK_CHECK("creating desc set layouts for compute pipeline", vkCreateDescriptorSetLayout(vk_device,
                &ci_desc_set_layout, nullptr, &vk_desc_set_layout));
        }

        //*** Pool
        {
            VkDescriptorPoolSize vk_desc_pool_size[1];
            vk_desc_pool_size[0].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
            vk_desc_pool_size[0].descriptorCount = 1;

            VkDescriptorPoolCreateInfo ci_desc_pool;
            ci_desc_pool.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
            ci_desc_pool.pNext = nullptr;
            ci_desc_pool.flags = 0x0;
            ci_desc_pool.maxSets = 1;
            ci_desc_pool.poolSizeCount = 1;
            ci_desc_pool.pPoolSizes = vk_desc_pool_size;

            VK_CHECK("creating desc pool for compute pipeline", vkCreateDescriptorPool(vk_device, &ci_desc_pool, nullptr,
                &vk_desc_pool));

            // allocate descs from pool
            VkDescriptorSetAllocateInfo ai_desc_set;
            ai_desc_set.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
            ai_desc_set.pNext = nullptr;
            ai_desc_set.descriptorPool = vk_desc_pool;
            ai_desc_set.descriptorSetCount = 1;
            ai_desc_set.pSetLayouts = &vk_desc_set_layout;

            VK_CHECK("allocating desc set for compute pipeline", vkAllocateDescriptorSets(vk_device, &ai_desc_set,
                &vk_desc_set));

            // connect storage buffer with the descriptor
            VkDescriptorBufferInfo vk_desc_buffer_info;
            vk_desc_buffer_info.buffer = storage_buffer.vk_buffer;
            vk_desc_buffer_info.offset = 0;
            vk_desc_buffer_info.range = storage_buffer.size;

            VkWriteDescriptorSet vk_write_desc_set;
            vk_write_desc_set.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            vk_write_desc_set.pNext = nullptr;
            vk_write_desc_set.dstSet = vk_desc_set;
            vk_write_desc_set.dstBinding = 0;
            vk_write_desc_set.dstArrayElement = 0;
            vk_write_desc_set.descriptorCount = 1;
            vk_write_desc_set.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
            vk_write_desc_set.pImageInfo = nullptr;
            vk_write_desc_set.pBufferInfo = &vk_desc_buffer_info;
            vk_write_desc_set.pTexelBufferView = nullptr;

            vkUpdateDescriptorSets(vk_device, 1, &vk_write_desc_set, 0, nullptr);
        }
    }
     
    //*** Compute Resource Creation
    VkPipeline vk_compute_pipeline = VK_NULL_HANDLE;
    VkPipelineLayout vk_compute_pipeline_layout = VK_NULL_HANDLE;

    {
        //*** Compute Pipeline creation
        {
            VkPushConstantRange vk_push_constant_range;
            vk_push_constant_range.offset = 0;
            vk_push_constant_range.size = sizeof(Push_Constant);
            vk_push_constant_range.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;

            VkPipelineShaderStageCreateInfo ci_pipeline_shader_stage;
            ci_pipeline_shader_stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            ci_pipeline_shader_stage.pNext = nullptr;
            ci_pipeline_shader_stage.flags = 0x0;
            ci_pipeline_shader_stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
            ci_pipeline_shader_stage.module = create_shader_module(vk_device, "../shaders/compute.comp.spv");
            ci_pipeline_shader_stage.pName = "main";
            ci_pipeline_shader_stage.pSpecializationInfo = nullptr;

            VkPipelineLayoutCreateInfo ci_compute_pipeline_layout;
            ci_compute_pipeline_layout.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            ci_compute_pipeline_layout.pNext = nullptr;
            ci_compute_pipeline_layout.flags = 0x0;
            ci_compute_pipeline_layout.setLayoutCount = 1;
            ci_compute_pipeline_layout.pSetLayouts = &vk_desc_set_layout;
            ci_compute_pipeline_layout.pushConstantRangeCount = 1;
            ci_compute_pipeline_layout.pPushConstantRanges = &vk_push_constant_range;

            VK_CHECK("creating compute pipeline layout", vkCreatePipelineLayout(vk_device, &ci_compute_pipeline_layout,
                nullptr, &vk_compute_pipeline_layout));

            VkComputePipelineCreateInfo ci_compute_pipeline;
            ci_compute_pipeline.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
            ci_compute_pipeline.pNext = nullptr;
            ci_compute_pipeline.flags = 0x0;
            ci_compute_pipeline.stage = ci_pipeline_shader_stage;
            ci_compute_pipeline.layout = vk_compute_pipeline_layout;
            ci_compute_pipeline.basePipelineHandle = VK_NULL_HANDLE;
            ci_compute_pipeline.basePipelineIndex = -1;

            VK_CHECK("compute pipeline creation", vkCreateComputePipelines(vk_device, VK_NULL_HANDLE, 1, 
                &ci_compute_pipeline, nullptr, &vk_compute_pipeline));

            vkDestroyShaderModule(vk_device, ci_pipeline_shader_stage.module, nullptr);
        }
    }


    //*** Graphics Resouce Creation
    VkPipeline vk_graphics_pipeline = VK_NULL_HANDLE;
    VkPipelineLayout vk_graphics_pipeline_layout = VK_NULL_HANDLE;

    VkClearValue vk_clearValues[2] = { 0 };
    VkRenderPassBeginInfo bi_render_pass;

    {
        //** Graphics Pipeline Creation
        {
            //*** Shader Stage
            VkPipelineShaderStageCreateInfo ci_pipeline_shader_stages[2];
            ci_pipeline_shader_stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            ci_pipeline_shader_stages[0].pNext = nullptr;
            ci_pipeline_shader_stages[0].flags = 0x0;
            ci_pipeline_shader_stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
            ci_pipeline_shader_stages[0].module = create_shader_module(vk_device, "../shaders/default-vert.spv");
            ci_pipeline_shader_stages[0].pName = "main";
            ci_pipeline_shader_stages[0].pSpecializationInfo = nullptr;

            ci_pipeline_shader_stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
            ci_pipeline_shader_stages[1].pNext = nullptr;
            ci_pipeline_shader_stages[1].flags = 0x0;
            ci_pipeline_shader_stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
            ci_pipeline_shader_stages[1].module = create_shader_module(vk_device, "../shaders/default-frag.spv");
            ci_pipeline_shader_stages[1].pName = "main";
            ci_pipeline_shader_stages[1].pSpecializationInfo = nullptr;

            //*** Input State
            VkVertexInputBindingDescription vk_input_binding_description;
            vk_input_binding_description.binding = 0;
            vk_input_binding_description.stride = sizeof(float) * 3;
            vk_input_binding_description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

            VkVertexInputAttributeDescription vk_input_attribute_descriptions[0]; //  = { 0 };

            // Pos (vec3)
            // vk_input_attribute_descriptions[0].location = 0;
            // vk_input_attribute_descriptions[0].binding = 0;
            // vk_input_attribute_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
            // vk_input_attribute_descriptions[0].offset = 0;

            // Norm (vec3)
            // vk_input_attribute_descriptions[1].location = 1;
            // vk_input_attribute_descriptions[1].binding = 0;
            // vk_input_attribute_descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
            // vk_input_attribute_descriptions[1].offset = sizeof(float) * 3;

            VkPipelineVertexInputStateCreateInfo ci_pipeline_vertex_input_state;
            ci_pipeline_vertex_input_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
            ci_pipeline_vertex_input_state.pNext = nullptr;
            ci_pipeline_vertex_input_state.flags = 0x0;
            ci_pipeline_vertex_input_state.vertexBindingDescriptionCount = 0;
            ci_pipeline_vertex_input_state.pVertexBindingDescriptions = nullptr; // &vk_input_binding_description;
            ci_pipeline_vertex_input_state.vertexAttributeDescriptionCount = 0;
            ci_pipeline_vertex_input_state.pVertexAttributeDescriptions = nullptr; // vk_input_attribute_descriptions;

            //*** Input Assembly
            VkPipelineInputAssemblyStateCreateInfo ci_pipeline_input_assembly_state;
            ci_pipeline_input_assembly_state.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
            ci_pipeline_input_assembly_state.pNext = nullptr;
            ci_pipeline_input_assembly_state.flags = 0x0;
            ci_pipeline_input_assembly_state.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST; // VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
            ci_pipeline_input_assembly_state.primitiveRestartEnable = VK_FALSE;

            //*** Tessellation State
            // VkPipelineTessellationStateCreateInfo ci_pipeline_tesselation_state;
            // ci_pipeline_tesselation_state.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
            // ci_pipeline_tesselation_state.pNext = nullptr;
            // ci_pipeline_tesselation_state.flags = 0x0;
            // ci_pipeline_tesselation_state.patchControlPoints = 0;

            //*** Viewport State
            VkViewport vk_viewport;
            vk_viewport.x = 0;
            vk_viewport.y = 0;
            vk_viewport.width = static_cast<float>(ci_swapchain.imageExtent.width);
            vk_viewport.height = static_cast<float>(ci_swapchain.imageExtent.height);
            vk_viewport.minDepth = 0.0f;
            vk_viewport.maxDepth = 1.0f;

            VkRect2D vk_rect2D_scissor;
            vk_rect2D_scissor.offset.x = 0;
            vk_rect2D_scissor.offset.y = 0;
            vk_rect2D_scissor.extent = ci_swapchain.imageExtent;

            VkPipelineViewportStateCreateInfo ci_pipeline_viewport_state;
            ci_pipeline_viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
            ci_pipeline_viewport_state.pNext = nullptr;
            ci_pipeline_viewport_state.flags = 0x0;
            ci_pipeline_viewport_state.viewportCount = 1;
            ci_pipeline_viewport_state.pViewports = &vk_viewport;
            ci_pipeline_viewport_state.scissorCount = 1;
            ci_pipeline_viewport_state.pScissors = &vk_rect2D_scissor;

            //*** Rasterization State
            VkPipelineRasterizationStateCreateInfo ci_pipeline_rasterization_state;
            ci_pipeline_rasterization_state.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
            ci_pipeline_rasterization_state.pNext = nullptr;
            ci_pipeline_rasterization_state.flags = 0x0;
            ci_pipeline_rasterization_state.depthClampEnable = VK_FALSE;
            ci_pipeline_rasterization_state.rasterizerDiscardEnable = VK_FALSE;
            ci_pipeline_rasterization_state.polygonMode = VK_POLYGON_MODE_FILL;
            ci_pipeline_rasterization_state.cullMode = VK_CULL_MODE_NONE;
            ci_pipeline_rasterization_state.frontFace = VK_FRONT_FACE_CLOCKWISE;
            ci_pipeline_rasterization_state.depthBiasEnable = VK_FALSE;
            ci_pipeline_rasterization_state.depthBiasConstantFactor = 0.f;
            ci_pipeline_rasterization_state.depthBiasClamp = 0.f;
            ci_pipeline_rasterization_state.depthBiasSlopeFactor = 0.f;
            ci_pipeline_rasterization_state.lineWidth = 1.f;

            //*** MultiSample State
            VkPipelineMultisampleStateCreateInfo ci_pipeline_multisample_state;
            ci_pipeline_multisample_state.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
            ci_pipeline_multisample_state.pNext = nullptr;
            ci_pipeline_multisample_state.flags = 0x0;
            ci_pipeline_multisample_state.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
            ci_pipeline_multisample_state.sampleShadingEnable = VK_FALSE;
            ci_pipeline_multisample_state.minSampleShading = 0;
            ci_pipeline_multisample_state.pSampleMask = nullptr;
            ci_pipeline_multisample_state.alphaToCoverageEnable = VK_FALSE;
            ci_pipeline_multisample_state.alphaToOneEnable = VK_FALSE;

            //*** DepthStencil State
            VkStencilOpState vk_stencil_op_state_front;
            vk_stencil_op_state_front.failOp = VK_STENCIL_OP_KEEP;
            vk_stencil_op_state_front.passOp = VK_STENCIL_OP_KEEP;
            vk_stencil_op_state_front.depthFailOp = VK_STENCIL_OP_KEEP;
            vk_stencil_op_state_front.compareOp = VK_COMPARE_OP_NEVER;
            vk_stencil_op_state_front.compareMask = 0x0;
            vk_stencil_op_state_front.writeMask = 0x0;
            vk_stencil_op_state_front.reference = 0x0;

            VkStencilOpState vk_stencil_op_state_back;
            vk_stencil_op_state_back.failOp = VK_STENCIL_OP_KEEP;
            vk_stencil_op_state_back.passOp = VK_STENCIL_OP_KEEP;
            vk_stencil_op_state_back.depthFailOp = VK_STENCIL_OP_KEEP;
            vk_stencil_op_state_back.compareOp = VK_COMPARE_OP_NEVER;
            vk_stencil_op_state_back.compareMask = 0x0;
            vk_stencil_op_state_back.writeMask = 0x0;
            vk_stencil_op_state_back.reference = 0x0;

            VkPipelineDepthStencilStateCreateInfo ci_pipeline_depth_stencil_state;
            ci_pipeline_depth_stencil_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
            ci_pipeline_depth_stencil_state.pNext = nullptr;
            ci_pipeline_depth_stencil_state.flags = 0x0;
            ci_pipeline_depth_stencil_state.depthTestEnable = VK_FALSE;
            ci_pipeline_depth_stencil_state.depthWriteEnable = VK_FALSE;
            ci_pipeline_depth_stencil_state.depthCompareOp = VK_COMPARE_OP_LESS;
            ci_pipeline_depth_stencil_state.depthBoundsTestEnable = VK_FALSE;
            ci_pipeline_depth_stencil_state.front = vk_stencil_op_state_front;
            ci_pipeline_depth_stencil_state.back = vk_stencil_op_state_back;
            ci_pipeline_depth_stencil_state.minDepthBounds = 0.f;
            ci_pipeline_depth_stencil_state.minDepthBounds = 1.f;
            ci_pipeline_depth_stencil_state.stencilTestEnable = VK_FALSE;

            //*** Blend State
            VkPipelineColorBlendAttachmentState vk_pipeline_color_blend_attachment_state;
            vk_pipeline_color_blend_attachment_state.blendEnable = VK_TRUE;
            vk_pipeline_color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_DST_COLOR;
            vk_pipeline_color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
            vk_pipeline_color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
            vk_pipeline_color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
            vk_pipeline_color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
            vk_pipeline_color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
            vk_pipeline_color_blend_attachment_state.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

            VkPipelineColorBlendStateCreateInfo ci_pipeline_color_blend_state;
            ci_pipeline_color_blend_state.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            ci_pipeline_color_blend_state.pNext = nullptr;
            ci_pipeline_color_blend_state.flags = 0x0;
            ci_pipeline_color_blend_state.logicOpEnable = VK_FALSE;
            ci_pipeline_color_blend_state.logicOp = VK_LOGIC_OP_COPY;
            ci_pipeline_color_blend_state.attachmentCount = 1;
            ci_pipeline_color_blend_state.pAttachments = &vk_pipeline_color_blend_attachment_state;
            ci_pipeline_color_blend_state.blendConstants[0] = 0;
            ci_pipeline_color_blend_state.blendConstants[1] = 0;
            ci_pipeline_color_blend_state.blendConstants[2] = 0;
            ci_pipeline_color_blend_state.blendConstants[3] = 0;

            //*** Dynamic State
            VkPipelineDynamicStateCreateInfo ci_pipeline_dynamic_state;
            ci_pipeline_dynamic_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
            ci_pipeline_dynamic_state.pNext = nullptr;
            ci_pipeline_dynamic_state.flags = 0x0;
            ci_pipeline_dynamic_state.dynamicStateCount = 0;
            ci_pipeline_dynamic_state.pDynamicStates = nullptr; // TODO - set this to viewportr and scissor for window resizing

            //*** Layout
            VkPipelineLayoutCreateInfo ci_pipeline_layout;
            ci_pipeline_layout.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            ci_pipeline_layout.pNext = nullptr;
            ci_pipeline_layout.flags = 0x0;
            ci_pipeline_layout.setLayoutCount = 1;
            ci_pipeline_layout.pSetLayouts = &vk_desc_set_layout;
            ci_pipeline_layout.pushConstantRangeCount = 0;
            ci_pipeline_layout.pPushConstantRanges = nullptr;

            VK_CHECK("creating graphics pipeline layout", vkCreatePipelineLayout(vk_device, &ci_pipeline_layout,
                nullptr, &vk_graphics_pipeline_layout));

            //*** Graphics Pipeline Creation
            VkGraphicsPipelineCreateInfo ci_graphicsPipeline;
            ci_graphicsPipeline.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            ci_graphicsPipeline.pNext = nullptr;
            ci_graphicsPipeline.flags = 0x0;
            ci_graphicsPipeline.stageCount = 2;
            ci_graphicsPipeline.pStages = ci_pipeline_shader_stages;
            ci_graphicsPipeline.pVertexInputState = &ci_pipeline_vertex_input_state;
            ci_graphicsPipeline.pInputAssemblyState = &ci_pipeline_input_assembly_state;
            // ci_graphicsPipeline.pTessellationState = &ci_pipeline_tesselation_state;
            ci_graphicsPipeline.pViewportState = &ci_pipeline_viewport_state;
            ci_graphicsPipeline.pRasterizationState = &ci_pipeline_rasterization_state;
            ci_graphicsPipeline.pMultisampleState = &ci_pipeline_multisample_state;
            ci_graphicsPipeline.pDepthStencilState = &ci_pipeline_depth_stencil_state;
            ci_graphicsPipeline.pColorBlendState = &ci_pipeline_color_blend_state;
            ci_graphicsPipeline.pDynamicState = &ci_pipeline_dynamic_state;
            ci_graphicsPipeline.layout = vk_graphics_pipeline_layout;
            ci_graphicsPipeline.renderPass = vk_render_pass;
            ci_graphicsPipeline.subpass = 0;
            ci_graphicsPipeline.basePipelineHandle = VK_NULL_HANDLE;
            ci_graphicsPipeline.basePipelineIndex = 0;

            VK_CHECK("creating graphics pipeline", vkCreateGraphicsPipelines(vk_device, VK_NULL_HANDLE, 1, 
                &ci_graphicsPipeline, nullptr, &vk_graphics_pipeline));

            vkDestroyShaderModule(vk_device, ci_pipeline_shader_stages[0].module, nullptr);
            vkDestroyShaderModule(vk_device, ci_pipeline_shader_stages[1].module, nullptr);
        }

        //** Init Code which stays the same from frame to frame
        {
            vk_clearValues[0].color = { 0.22f, 0.22f, 0.22f, 1.0f };
            vk_clearValues[1].depthStencil = { 1.0f, 0 };

            bi_render_pass.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
            bi_render_pass.pNext = nullptr;
            bi_render_pass.renderPass = vk_render_pass;
            bi_render_pass.renderArea.extent = ci_swapchain.imageExtent;
            bi_render_pass.renderArea.offset = { 0, 0 };
            bi_render_pass.clearValueCount = 2;
            bi_render_pass.pClearValues = vk_clearValues;
        }
    }


    //*** ---------- Render Loop ---------- ***// 

    uint32_t curr_frame_resource_index = 0;
    uint32_t curr_swapchain_image_index = 0;


    int frame = 0;
    double prev_time = glfwGetTime();
    double frame_time;
    while (!glfwWindowShouldClose(glfw_window))
    {
        glfwPollEvents();
        push_constant.time = glfwGetTime();

        double tmp_time = glfwGetTime();
        frame_time += tmp_time - prev_time;
        prev_time = tmp_time;
        frame++;

        if (frame == 60)
        {
            printf("frame time: %f\n", frame_time / 60);
            frame = 0;
            frame_time = 0;
        }

        //*** Wait for graphics work from the previous frame to complete
        VK_CHECK("waiting for graphics fence", vkWaitForFences(vk_device, 1, &vk_graphics_fence, 
            VK_TRUE, UINT64_MAX));
        
        VK_CHECK("reseting graphics fence", vkResetFences(vk_device, 1, &vk_graphics_fence));

        //*** Wait for present work from the previous frame to complete
        VK_CHECK("acquiring swapchain image", vkAcquireNextImageKHR(vk_device, vk_swapchain, 
            UINT64_MAX, vk_graphics_sem_image_acquired, VK_NULL_HANDLE, &curr_swapchain_image_index));

        //*** Compute Record
        {
            VK_CHECK("compute command buffer begin", vkBeginCommandBuffer(vk_compute_cmd_buffer,
                &bi_compute_cmd_buffer));

                vkCmdBindPipeline(vk_compute_cmd_buffer, VK_PIPELINE_BIND_POINT_COMPUTE, vk_compute_pipeline);
                vkCmdPushConstants(vk_compute_cmd_buffer, vk_compute_pipeline_layout,
                    VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(Push_Constant), &push_constant);
                vkCmdBindDescriptorSets(vk_compute_cmd_buffer, VK_PIPELINE_BIND_POINT_COMPUTE,
                    vk_compute_pipeline_layout, 0, 1, &vk_desc_set, 0, nullptr);

                vkCmdDispatch(vk_compute_cmd_buffer, num_workgroups, 1, 1);
            VK_CHECK("compute command buffer end", vkEndCommandBuffer(vk_compute_cmd_buffer));
        }

        //*** Graphic Record
        {
            vkResetCommandPool(vk_device, vk_graphics_cmd_pool, 0x0);

            bi_render_pass.framebuffer = vk_framebuffer_list[curr_frame_resource_index];

            VK_CHECK("graphics command buffer begin", vkBeginCommandBuffer(vk_graphics_cmd_buffer,
                &bi_graphics_cmd_buffer));

                const VkBufferMemoryBarrier vk_buffer_memory_barrier = {
                    .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
                    .pNext = nullptr,
                    .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT | VK_ACCESS_SHADER_WRITE_BIT,
                    .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
                    .srcQueueFamilyIndex = compute_q_index,
                    .dstQueueFamilyIndex = graphics_q_index,
                    .buffer = storage_buffer.vk_buffer,
                    .offset = 0,
                    .size = storage_buffer.size
                };

                vkCmdPipelineBarrier(vk_graphics_cmd_buffer, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 
                    VK_DEPENDENCY_BY_REGION_BIT, 0, nullptr, 0, &vk_buffer_memory_barrier, 0, nullptr);

                vkCmdBeginRenderPass(vk_graphics_cmd_buffer, &bi_render_pass, VK_SUBPASS_CONTENTS_INLINE);

                vkCmdBindPipeline(vk_graphics_cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vk_graphics_pipeline);
                vkCmdBindDescriptorSets(vk_graphics_cmd_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vk_graphics_pipeline_layout, 0, 1, &vk_desc_set, 0, nullptr);

                vkCmdDraw(vk_graphics_cmd_buffer, num_particles, 1, 0, 0);

                vkCmdEndRenderPass(vk_graphics_cmd_buffer);

            VK_CHECK("graphics command buffer end", vkEndCommandBuffer(vk_graphics_cmd_buffer));
        }

        //*** Submission/Presentation
        {
            //*** Submit Compute Work
            VkSubmitInfo vk_submit_info;
            vk_submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            vk_submit_info.pNext = nullptr;
            vk_submit_info.waitSemaphoreCount = 0;
            vk_submit_info.pWaitSemaphores = nullptr;
            vk_submit_info.commandBufferCount = 1;
            vk_submit_info.pCommandBuffers = &vk_compute_cmd_buffer;
            vk_submit_info.signalSemaphoreCount = 1;
            vk_submit_info.pSignalSemaphores = &vk_compute_sem_work_done;

            VK_CHECK("compute submission", vkQueueSubmit(vk_compute_q, 1, &vk_submit_info, VK_NULL_HANDLE));

            //*** Submit Graphics Work (wait for compute work to finish)
            const VkPipelineStageFlags waitStageMask[2] = { VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
            const VkSemaphore waitSemaphores[2] = { vk_compute_sem_work_done, vk_graphics_sem_image_acquired };
            VkSubmitInfo vk_graphics_submit_info;
            vk_graphics_submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            vk_graphics_submit_info.pNext = nullptr;
            vk_graphics_submit_info.waitSemaphoreCount = 2;
            vk_graphics_submit_info.pWaitSemaphores = waitSemaphores;
            vk_graphics_submit_info.pWaitDstStageMask = waitStageMask;
            vk_graphics_submit_info.commandBufferCount = 1;
            vk_graphics_submit_info.pCommandBuffers = &vk_graphics_cmd_buffer;
            vk_graphics_submit_info.signalSemaphoreCount = 1;
            vk_graphics_submit_info.pSignalSemaphores = &vk_graphics_sem_render_done;

            VK_CHECK("graphics submission", vkQueueSubmit(vk_graphics_q, 1, &vk_graphics_submit_info, 
                vk_graphics_fence));

            //*** Present (wait for graphics work to complete)
            VkPresentInfoKHR vk_present_info;
            vk_present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
            vk_present_info.pNext = nullptr;
            vk_present_info.waitSemaphoreCount = 1;
            vk_present_info.pWaitSemaphores = &vk_graphics_sem_render_done;
            vk_present_info.swapchainCount = 1;
            vk_present_info.pSwapchains = &vk_swapchain;
            vk_present_info.pImageIndices = &curr_swapchain_image_index;
            vk_present_info.pResults = nullptr;

            VK_CHECK("presenting swapchain image", vkQueuePresentKHR(vk_graphics_q, &vk_present_info));
        }

        curr_frame_resource_index = (curr_frame_resource_index + 1) % NUM_FRAMES_IN_FLIGHT;

        // vkDeviceWaitIdle(vk_device);
    }

    vkDeviceWaitIdle(vk_device);


    //*** Teardown
    {
        vkDestroyFence(vk_device, vk_graphics_fence, nullptr);
        vkDestroySemaphore(vk_device, vk_graphics_sem_image_acquired, nullptr);
        vkDestroySemaphore(vk_device, vk_graphics_sem_render_done, nullptr);
        vkDestroySemaphore(vk_device, vk_compute_sem_work_done, nullptr);
        vkDestroyCommandPool(vk_device, vk_graphics_cmd_pool, nullptr);

        for (size_t i = 0; i < NUM_FRAMES_IN_FLIGHT; ++i)
        {
           vkDestroyFramebuffer(vk_device, vk_framebuffer_list[i], nullptr);
        }

        vkDestroyRenderPass(vk_device, vk_render_pass, nullptr);

        for (uint32_t i = 0; i < num_swapchain_images; ++i)
        {
            vkDestroyImageView(vk_device, vk_swapchain_image_view_list[i], nullptr);
        }

        delete [] vk_swapchain_image_list;
        delete [] vk_swapchain_image_view_list;

        vkDestroySwapchainKHR(vk_device, vk_swapchain, nullptr);

        vkDestroyCommandPool(vk_device, vk_compute_cmd_pool, nullptr);

        vkDestroyPipelineLayout(vk_device, vk_compute_pipeline_layout, nullptr);
        vkDestroyPipeline(vk_device, vk_compute_pipeline, nullptr);
        vkDestroyPipelineLayout(vk_device, vk_graphics_pipeline_layout, nullptr);
        vkDestroyPipeline(vk_device, vk_graphics_pipeline, nullptr);

        vkDestroyDescriptorPool(vk_device, vk_desc_pool, nullptr);
        vkDestroyDescriptorSetLayout(vk_device, vk_desc_set_layout, nullptr);

        vkFreeMemory(vk_device, storage_buffer.vk_device_memory, nullptr);
        vkDestroyBuffer(vk_device, storage_buffer.vk_buffer, nullptr);

        vkDestroyDevice(vk_device, nullptr);
        vkDestroySurfaceKHR(vk_instance, vk_surface, nullptr);
        vkDestroyInstance(vk_instance, nullptr);

        glfwDestroyWindow(glfw_window);
        glfwTerminate();
        
    }

    return 0;
}