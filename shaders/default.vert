#version 450

struct Particle
{
    vec4 col;
    vec2 pos;
    vec2 vel;
};

layout(std140, set = 0, binding = 0) readonly buffer compute_buffer
{
    Particle particles[];
};

layout(location = 0) out vec4 frag_color;

void main() {
    gl_PointSize = 1;
    gl_Position = vec4(particles[gl_VertexIndex].pos, 0.f, 1.f);

    frag_color = particles[gl_VertexIndex].col;
}