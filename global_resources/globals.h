#ifndef GLOBALS_H
#define GLOBALS_H

#include <vulkan/vulkan.h>

VkDevice vk_device;
VkPhysicalDeviceMemoryProperties vk_phys_dev_mem_props;

#endif // GLOBALS_H