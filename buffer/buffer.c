#include "buffer.h"

#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

#define VK_CHECK( str, result ) do { \
   if (result != VK_SUCCESS) \
    printf("%s: result != VK_SUCCESS", str); \
} while ( 0 ) \


static
uint32_t
get_memory_type_index(const VkPhysicalDeviceMemoryProperties* vk_phys_dev_mem_props, const uint32_t typeBits, const VkMemoryPropertyFlags properties)
{
	// Iterate over all memory types available for the device used in this example
	for (uint32_t i = 0; i < vk_phys_dev_mem_props->memoryTypeCount; i++)
	{
		if (typeBits & (1 << i) && (vk_phys_dev_mem_props->memoryTypes[i].propertyFlags & properties) == properties)
		{
			return i;
		}
	}

    assert( false && "Could not find suitable memory type!");
}

void
create_buffer(const VkDevice vk_device, const VkPhysicalDeviceMemoryProperties* vk_phys_dev_mem_props, const VkBufferUsageFlags usage, const VkMemoryPropertyFlags properties, struct Buffer* buffer)
{
    const VkBufferCreateInfo ci_buffer = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size  = buffer->size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE 
    };

    VK_CHECK("vkCreateBuffer()", vkCreateBuffer(vk_device, &ci_buffer, NULL, &(buffer->vk_buffer)));

    VkMemoryRequirements vk_mem_reqs;
    vkGetBufferMemoryRequirements(vk_device, buffer->vk_buffer, &vk_mem_reqs);

    const VkMemoryAllocateInfo ai_memory = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize  = vk_mem_reqs.size,
        .memoryTypeIndex = get_memory_type_index(vk_phys_dev_mem_props, vk_mem_reqs.memoryTypeBits, properties),
    };
    
    VK_CHECK("vkAllocateMemory()", vkAllocateMemory(vk_device, &ai_memory, NULL,
        &buffer->vk_device_memory));

    VK_CHECK("vkBindBufferMemory()", vkBindBufferMemory(vk_device,
        buffer->vk_buffer, buffer->vk_device_memory, 0));
}
