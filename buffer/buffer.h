#ifndef HELPERS_H
#define HELPERS_H

#include <vulkan/vulkan.h>

struct Buffer
{
    VkBuffer vk_buffer;
    VkDeviceMemory vk_device_memory;

    VkDeviceSize size;
    VkDeviceSize offset;
};

void
create_buffer(const VkDevice vk_device, const VkPhysicalDeviceMemoryProperties* vk_phys_dev_mem_props, const VkBufferUsageFlags usage, const VkMemoryPropertyFlags properties, struct Buffer* buffer);

#endif // HELPERS_H